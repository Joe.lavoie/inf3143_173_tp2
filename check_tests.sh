#!/bin/bash
#
# Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

out=out
build_file=$out/tests.build

mkdir -p out

make build-tests -s > $build_file 2>&1

# Check empty output
if [ -s $build_file ]; then
	echo -ne "[\e[31mERR\e[0m] "
	echo -ne "\e[1mCOMPILING JUNITS\e[0m "
	echo -e "(cat $build_file)"
	exit 1
fi

# Run tests
for test_path in `ls Mages/test/mages/*.java`; do
	test_file=`basename $test_path`
	test=${test_file%.*}
	res_file=$out/$test.out
	echo -ne " * "

	java -cp bin/:Mages/lib/junit-4.12.jar:Mages/lib/hamcrest-core-1.3.jar org.junit.runner.JUnitCore mages.$test > $res_file 2>&1

	if [ $? -ne 0 ]; then
		echo -ne "[\e[31mKO\e[0m] "
		echo -ne "\e[1m$test\e[0m "
		echo -e "(cat $res_file)"
	else
		echo -ne "[\e[32mOK\e[0m] "
		echo -e "\e[1m$test\e[0m"
	fi
done
