/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package contrats;

import mages.Mage;

public class TestMage15 {
   
    public static void main(String[] args) {
        Mage mage = new Mage15("test", 1);
        mage.setVie(90);
        mage.blesse(10);
    }
}

class Mage15 extends Mage {

    public Mage15(String nom, Integer niveau) {
        super(nom, niveau);
    }

    @Override
    public void blesse(Integer vie) {
        this.setVie(0);
    }

}