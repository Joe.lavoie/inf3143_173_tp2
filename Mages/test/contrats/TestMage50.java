/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package contrats;

import java.util.HashSet;
import java.util.Set;
import mages.Mage;
import mages.Rune;
import mages.RuneCible;
import mages.RuneDommage;
import mages.Sac;
import mages.SacException;
import mages.Sort;

public class TestMage50 {

    public static void main(String[] args) throws SacException {
        Mage mage = new Mage("test", 1);
        mage.equipe(new Sac(200));
        mage.setMana(10);

        Rune r1 = new RuneCible();
        Rune r2 = new RuneCible();
        Rune r3 = new RuneDommage(10);

        mage.ramasse(r1);
        mage.ramasse(r2);
        mage.ramasse(r3);

        Set<Rune> runes = new HashSet<>();
        runes.add(r1);
        runes.add(r2);
        runes.add(r3);

        Set<Mage> cibles = new HashSet<>();
        cibles.add(new Mage("m2", 1));
        cibles.add(new Mage("m3", 1));

        Sort sort = mage.prepareSort(runes, cibles);
        mage.lanceSort(sort);
    }
}
